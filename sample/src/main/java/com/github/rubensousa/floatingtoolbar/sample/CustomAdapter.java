package com.github.rubensousa.floatingtoolbar.sample;

import com.github.rubensousa.floatingtoolbar.MenuItem;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;


public class CustomAdapter extends BaseItemProvider {

    private Context mContext;
    private ClickListener mListener;
    private List<MenuItem> mItems;

    public CustomAdapter(Context context, ClickListener listener) {
        mContext = context;
        mItems = new ArrayList<>();
        mListener = listener;
    }

    public void setClickListener(ClickListener listener) {
        mListener = listener;
    }

    public void addItem(MenuItem item) {
        mItems.add(item);
        notifyDataSetItemInserted(mItems.size() - 1);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder = null;
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_adapter, null, false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (Text) component.findComponentById(ResourceTable.Id_textView);
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }

        viewHolder.textView.setText(mItems.get(i).getTitle().toString());

        component.setClickedListener(component1 -> {
            if (mListener != null) {
                mListener.onAdapterItemClick(mItems.get(i));
            }
        });
        return component;
    }

    public class ViewHolder {

        public MenuItem item;
        public Text textView;

    }

    public interface ClickListener {
        void onAdapterItemClick(MenuItem item);
    }
}
