package com.github.rubensousa.floatingtoolbar.sample;

import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.github.rubensousa.floatingtoolbar.FloatingToolbarMenuBuilder;
import com.github.rubensousa.floatingtoolbar.MenuItem;
import com.github.rubensousa.floatingtoolbar.Snackbar;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.VectorElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbility extends Ability implements FloatingToolbar.ItemClickListener,
        CustomAdapter.ClickListener, FloatingToolbar.MorphListener {

    private FloatingToolbar mFloatingToolbar;
    private CustomAdapter mAdapter;
    private Snackbar mSnackbar;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setStatusBarColor();
        setUIContent(ResourceTable.Layout_ability_main);
        Image fab = (Image) findComponentById(ResourceTable.Id_fab);
        ListContainer recyclerView = (ListContainer) findComponentById(ResourceTable.Id_recyclerView);
        mFloatingToolbar = (FloatingToolbar) findComponentById(ResourceTable.Id_floatingToolbar);
        mSnackbar = (Snackbar) findComponentById(ResourceTable.Id_snackbar);

        mAdapter = new CustomAdapter(this, this);
        recyclerView.setItemProvider(mAdapter);

        mFloatingToolbar.setSnackBar(mSnackbar);
        mFloatingToolbar.setClickListener(this);
        mFloatingToolbar.attachFab(fab);
        mFloatingToolbar.attachRecyclerView(recyclerView);
        mFloatingToolbar.addMorphListener(this);

        //Create a custom menu
        mFloatingToolbar.setMenu(new FloatingToolbarMenuBuilder(this)
                .addItem(0, new VectorElement(this, ResourceTable.Graphic_ic_markunread_black_24dp), "Mark unread")
                .addItem(1, new VectorElement(this, ResourceTable.Graphic_ic_content_copy_black_24dp), "Copy")
                .addItem(2, new VectorElement(this, ResourceTable.Graphic_ic_google_plus_box), "Google+")
                .addItem(3, new VectorElement(this, ResourceTable.Graphic_ic_facebook_box), "Facebook")
                .addItem(4, new VectorElement(this, ResourceTable.Graphic_ic_twitter_box), "Twitter")
                .build());

        findComponentById(ResourceTable.Id_snackbarText).setClickedListener(component -> {
            mFloatingToolbar.showSnackBar(mSnackbar);
        });
    }

    private void setStatusBarColor() {
        try {
            getWindow().setStatusBarColor(getResourceManager().getElement(ResourceTable.Color_colorPrimary).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        mFloatingToolbar.removeMorphListener(this);
    }

    @Override
    public void onMorphEnd() {

    }

    @Override
    public void onMorphStart() {

    }

    @Override
    public void onUnmorphStart() {

    }

    @Override
    public void onUnmorphEnd() {

    }

    @Override
    public void onItemClick(MenuItem item) {
        mAdapter.addItem(item);
    }

    @Override
    public void onItemLongClick(MenuItem item) {

    }

    @Override
    public void onAdapterItemClick(MenuItem item) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(DetailAbility.class.getName())
                .build();
        intent.setOperation(operation);
        startAbilityForResult(intent, 0);
    }
}
