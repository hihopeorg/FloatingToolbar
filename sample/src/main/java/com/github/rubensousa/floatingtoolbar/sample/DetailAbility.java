/*
 * Copyright 2016 Rúben Sousa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar.sample;


import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.github.rubensousa.floatingtoolbar.FloatingToolbarMenuBuilder;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.VectorElement;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class DetailAbility extends Ability implements FloatingToolbar.MorphListener {

    private Image mFabAppBar;
    private Image mFab;
    private FloatingToolbar mFloatingToolbar;
    private boolean mShowingFromAppBar;
    private boolean mShowingFromNormal;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setStatusBarColor();
        setUIContent(ResourceTable.Layout_detail_ability);

        Window window = getWindow();
        window.setStatusBarVisibility(Component.HIDE);
        window.setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);

        mFloatingToolbar = (FloatingToolbar) findComponentById(ResourceTable.Id_floatingToolbar);
        mFabAppBar = (Image) findComponentById(ResourceTable.Id_fab);
        mFab = (Image) findComponentById(ResourceTable.Id_fab2);

        // Don't handle fab click since we'll have 2 of them
        mFloatingToolbar.handleFabClick(false);
        mFloatingToolbar.addMorphListener(this);
        mFloatingToolbar.setMenu(new FloatingToolbarMenuBuilder(this)
                .addItem(0, new VectorElement(this, ResourceTable.Graphic_ic_markunread_black_24dp), "Mark unread")
                .addItem(1, new VectorElement(this, ResourceTable.Graphic_ic_content_copy_black_24dp), "Copy")
                .addItem(2, new VectorElement(this, ResourceTable.Graphic_ic_google_plus_box), "Google+")
                .addItem(3, new VectorElement(this, ResourceTable.Graphic_ic_facebook_box), "Facebook")
                .addItem(4, new VectorElement(this, ResourceTable.Graphic_ic_twitter_box), "Twitter")
                .build());

        mFab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component view) {
                mFloatingToolbar.attachFab(mFab);
                mFloatingToolbar.show();
                mShowingFromNormal = true;
            }
        });

        mFabAppBar.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component view) {
                if (!mFloatingToolbar.isShowing()) {
                    mFab.setVisibility(Component.HIDE);
                    mFloatingToolbar.attachFab(mFabAppBar);
                    mFloatingToolbar.show();
                    mShowingFromAppBar = true;
                }
            }
        });
    }

    private void setStatusBarColor() {
        try {
            getWindow().setStatusBarColor(getResourceManager().getElement(ResourceTable.Color_colorPrimary).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        mFloatingToolbar.removeMorphListener(this);
    }

    @Override
    public void onMorphEnd() {

    }

    @Override
    public void onMorphStart() {

    }

    @Override
    public void onUnmorphEnd() {
        if (mShowingFromAppBar) {
            mFab.setVisibility(Component.VISIBLE);
        }

        mShowingFromAppBar = false;
        mShowingFromNormal = false;
    }

    @Override
    public void onUnmorphStart() {

    }
}
