/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar.sample;

import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.github.rubensousa.floatingtoolbar.FloatingToolbarMenuBuilder;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.Assert;
import org.junit.Test;

public class ExampleOhosTest extends TestCase {

    private IAbilityDelegator mIAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    private MainAbility mAbility;
    private FloatingToolbar mFloatingToolbar;
    private Image mFab;

    @Override
    protected void setUp() throws Exception {
        mContext = mIAbilityDelegator.getAppContext();
        mAbility = EventHelper.startAbility(MainAbility.class);

        mFloatingToolbar = (FloatingToolbar) mAbility.findComponentById(ResourceTable.Id_floatingToolbar);
        mFab = (Image) mAbility.findComponentById(ResourceTable.Id_fab);
        mFloatingToolbar.attachFab(mFab);
        mFloatingToolbar.setMenu(new FloatingToolbarMenuBuilder(mContext)
                .addItem(0, new VectorElement(mContext, ResourceTable.Graphic_ic_markunread_black_24dp), "Mark unread")
                .addItem(1, new VectorElement(mContext, ResourceTable.Graphic_ic_content_copy_black_24dp), "Copy")
                .addItem(2, new VectorElement(mContext, ResourceTable.Graphic_ic_google_plus_box), "Google+")
                .addItem(3, new VectorElement(mContext, ResourceTable.Graphic_ic_facebook_box), "Facebook")
                .addItem(4, new VectorElement(mContext, ResourceTable.Graphic_ic_twitter_box), "Twitter")
                .build());

        EventHelper.waitForActive(mAbility, 5);
        Thread.sleep(3000L);
    }

    @Override
    protected void tearDown() throws Exception {
        Thread.sleep(2000);
        mIAbilityDelegator.stopAbility(mAbility);
        EventHelper.clearAbilities();
    }

    private void simulateClickOnScreen(Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;
        simulateClickOnScreen(pos[0], pos[1]);
    }

    private void simulateClickOnScreen(int x, int y) {
        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, x, y);
        mIAbilityDelegator.triggerTouchEvent(mAbility, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, x, y);
        mIAbilityDelegator.triggerTouchEvent(mAbility, ev2);
    }

    @Test
    public void showFloatingToolbar() throws InterruptedException {
        EventHelper.triggerClickEvent(mAbility, mFab);
        Thread.sleep(3000L);
        Assert.assertTrue(mFloatingToolbar.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void hideFloatingToolbar() throws InterruptedException {
        DirectionalLayout directionalLayout = (DirectionalLayout) mFloatingToolbar.getMenuLayout().getComponentAt(0);
        simulateClickOnScreen(directionalLayout);
        Thread.sleep(3000L);
        Assert.assertTrue(mFloatingToolbar.getVisibility() == Component.HIDE);
    }

}