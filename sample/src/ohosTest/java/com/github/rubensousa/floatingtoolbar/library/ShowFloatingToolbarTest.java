/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar.library;

import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.github.rubensousa.floatingtoolbar.FloatingToolbarMenuBuilder;
import com.github.rubensousa.floatingtoolbar.sample.EventHelper;
import com.github.rubensousa.floatingtoolbar.sample.MainAbility;
import com.github.rubensousa.floatingtoolbar.sample.ResourceTable;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class ShowFloatingToolbarTest extends TestCase {

    @Test
    public void testShowFloatingToolbar() throws InterruptedException {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        FloatingToolbar floatingToolbar = (FloatingToolbar) ability.findComponentById(ResourceTable.Id_floatingToolbar);
        floatingToolbar.setMenu(new FloatingToolbarMenuBuilder(context)
                .addItem(0, new VectorElement(context, ResourceTable.Graphic_ic_markunread_black_24dp), "Mark unread")
                .addItem(1, new VectorElement(context, ResourceTable.Graphic_ic_content_copy_black_24dp), "Copy")
                .addItem(2, new VectorElement(context, ResourceTable.Graphic_ic_google_plus_box), "Google+")
                .addItem(3, new VectorElement(context, ResourceTable.Graphic_ic_facebook_box), "Facebook")
                .addItem(4, new VectorElement(context, ResourceTable.Graphic_ic_twitter_box), "Twitter")
                .build());
        floatingToolbar.show();
        Thread.sleep(2000L);
        Assert.assertTrue(floatingToolbar.getVisibility() == Component.VISIBLE);
    }

}