/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar.library;

import com.github.rubensousa.floatingtoolbar.FloatingToolbar;
import com.github.rubensousa.floatingtoolbar.Snackbar;
import com.github.rubensousa.floatingtoolbar.sample.EventHelper;
import com.github.rubensousa.floatingtoolbar.sample.MainAbility;
import com.github.rubensousa.floatingtoolbar.sample.ResourceTable;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class ShowSnackbarTest extends TestCase {

    @Test
    public void testShowSnackbar() throws InterruptedException {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        FloatingToolbar floatingToolbar = (FloatingToolbar) ability.findComponentById(ResourceTable.Id_floatingToolbar);
        Snackbar snackbar = (Snackbar) ability.findComponentById(ResourceTable.Id_snackbar);

        floatingToolbar.showSnackBar(snackbar);
        Thread.sleep(2000L);
        Assert.assertTrue(floatingToolbar.getVisibility() == Component.VISIBLE);
    }

}