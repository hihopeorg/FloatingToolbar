# FloatingToolbar

**本项目是基于开源项目FloatingToolbar进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/rubensousa/FloatingToolbar ）追踪到原项目版本**

#### 项目介绍

- 项目名称：底部工具栏
- 所属系列：ohos的第三方组件适配移植
- 功能：从浮动操作按钮中变出的工具栏
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/rubensousa/FloatingToolbar
- 原项目基线版本：v1.5.1 , sha1:c5fe81266cb5bb2b6004d9dd1d98a0a0d16104f1
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="preview/preview.gif"/>

#### 安装教程

方法1.

1. 编译har包FloatingToolbar.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.github.rubensousa.ohos:floatingtoolbar:1.0.0'
}
```

#### 使用说明

##### 1. 添加布局:
```xml
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:id="$+id:coordinatorLayout"
    ohos:height="match_parent"
    ohos:width="match_parent">

    <DependentLayout
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:above="$id:snackbar">

        <com.github.rubensousa.floatingtoolbar.FloatingToolbar
            ohos:id="$+id:floatingToolbar"
            ohos:height="50fp"
            ohos:width="match_parent"
            />

        <Image
            ohos:id="$+id:fab"
            ohos:height="40vp"
            ohos:width="40vp"
            ohos:align_parent_right="true"
            ohos:background_element="$graphic:background_fab"
            ohos:image_src="$graphic:ic_share_black_24dp"
            ohos:margin="$float:fab_margin"/>
    </DependentLayout>

    <com.github.rubensousa.floatingtoolbar.Snackbar
        ohos:id="$+id:snackbar"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:align_parent_bottom="true"
        ohos:visibility="hide"/>
</DependentLayout>
```

##### 2. 添加菜单到FloatingToolbar
```
mFloatingToolbar.setMenu(new FloatingToolbarMenuBuilder(this)
                .addItem(0, new VectorElement(this, ResourceTable.Graphic_ic_markunread_black_24dp), "Mark unread")
                .addItem(1, new VectorElement(this, ResourceTable.Graphic_ic_content_copy_black_24dp), "Copy")
                .addItem(2, new VectorElement(this, ResourceTable.Graphic_ic_google_plus_box), "Google+")
                .addItem(3, new VectorElement(this, ResourceTable.Graphic_ic_facebook_box), "Facebook")
                .addItem(4, new VectorElement(this, ResourceTable.Graphic_ic_twitter_box), "Twitter")
                .build());
```

##### 3. 将FAB连接到浮动工具栏，以便在单击事件时自动启动转换
```
mFloatingToolbar.attachFab(fab);
```

##### 4. 设置单击侦听器
```
mFloatingToolbar.setClickListener(new FloatingToolbar.ItemClickListener() {
            @Override
            public void onItemClick(MenuItem item) {
                
            }

            @Override
            public void onItemLongClick(MenuItem item) {

            }
        });
```

##### 5. 如果您想显示Snackbar，请使用:
```
mFloatingToolbar.showSnackBar(snackbar);
```

##### 6. 滚动ListContainer隐藏FloatingToolbar
```
mFloatingToolbar.attachRecyclerView(recyclerView);
```

##### 7. 使用show()和hide()显示隐藏FloatingToolbar
```
mFloatingToolbar.show();
mFloatingToolbar.hide();
```      

#### 版本迭代
- v1.0.0
- 已实现功能
    1. 点击浮动按钮显示自定义底部工具栏，隐藏浮动按钮
    2. 点击自定义底部工具栏任意菜单隐藏底部工具栏，显示浮动按钮

#### 版权和许可信息

    Copyright 2016 Rúben Sousa
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
