package com.github.rubensousa.floatingtoolbar;


import ohos.agp.components.Component;

class FloatingSnackBarManager implements FloatingToolbar.MorphListener {

    Snackbar mSnackBar;
    FloatingToolbar mFloatingToolbar;

    public FloatingSnackBarManager(FloatingToolbar toolbar) {
        mFloatingToolbar = toolbar;
        mFloatingToolbar.addMorphListener(this);
    }

    public void showSnackBar(final Snackbar snackbar) {
        // If we're currently morphing, show the snackbar after
        if (mFloatingToolbar.mAnimating) {
            mSnackBar = snackbar;
        } else {
            showSnackBarInternal(snackbar);
        }
    }

    private void showSnackBarInternal(Snackbar snackbar) {
        mSnackBar = snackbar;
        // We can show the snackbar now if the toolbar isn't showing
        if (!mFloatingToolbar.isShowing()) {
            mSnackBar.setVisibility(Component.VISIBLE);
            return;
        }
        mSnackBar.setVisibility(Component.VISIBLE);
    }

    @Override
    public void onMorphEnd() {

    }

    @Override
    public void onMorphStart() {

    }

    @Override
    public void onUnmorphStart() {

    }

    @Override
    public void onUnmorphEnd() {

    }
}
