/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.app.Context;

import java.util.ArrayList;

public class MenuBuilder implements SupportMenu {
    private static final String TAG = "MenuBuilder";
    private static final int[] sCategoryToOrder = new int[]{1, 4, 5, 3, 2, 0};
    private final Context mContext;
    private boolean mQwertyMode;
    private boolean mShortcutsVisible;
    private MenuBuilder.Callback mCallback;
    private ArrayList<MenuItemImpl> mItems;
    private ArrayList<MenuItemImpl> mVisibleItems;
    private boolean mIsVisibleItemsStale;
    private ArrayList<MenuItemImpl> mActionItems;
    private ArrayList<MenuItemImpl> mNonActionItems;
    private boolean mIsActionItemsStale;
    private int mDefaultShowAsAction = 0;
    private ContextMenu.ContextMenuInfo mCurrentMenuInfo;
    CharSequence mHeaderTitle;
    Element mHeaderIcon;
    Component mHeaderView;
    private boolean mPreventDispatchingItemsChanged = false;
    private boolean mItemsChangedWhileDispatchPrevented = false;
    private boolean mStructureChangedWhileDispatchPrevented = false;
    private boolean mOptionalIconsVisible = false;
    private boolean mIsClosing = false;
    private ArrayList<MenuItemImpl> mTempShortcutItemList = new ArrayList();
    private MenuItemImpl mExpandedItem;
    private boolean mOverrideVisibleItems;

    public MenuBuilder(Context context) {
        this.mContext = context;
        this.mItems = new ArrayList();
        this.mVisibleItems = new ArrayList();
        this.mIsVisibleItemsStale = true;
        this.mActionItems = new ArrayList();
        this.mNonActionItems = new ArrayList();
        this.mIsActionItemsStale = true;
    }

    public MenuBuilder setDefaultShowAsAction(int defaultShowAsAction) {
        this.mDefaultShowAsAction = defaultShowAsAction;
        return this;
    }

    public void setCallback(MenuBuilder.Callback cb) {
        this.mCallback = cb;
    }

    protected MenuItem addInternal(int group, int id, int categoryOrder, CharSequence title) {
        int ordering = getOrdering(categoryOrder);
        MenuItemImpl item = this.createNewMenuItem(group, id, categoryOrder, ordering, title, this.mDefaultShowAsAction);
        if (this.mCurrentMenuInfo != null) {
            item.setMenuInfo(this.mCurrentMenuInfo);
        }

        this.mItems.add(findInsertIndex(this.mItems, ordering), item);
        return item;
    }

    private MenuItemImpl createNewMenuItem(int group, int id, int categoryOrder, int ordering, CharSequence title, int defaultShowAsAction) {
        return new MenuItemImpl(this, group, id, categoryOrder, ordering, title, defaultShowAsAction);
    }

    public MenuItem add(CharSequence title) {
        return this.addInternal(0, 0, 0, title);
    }

    public MenuItem add(int group, int id, int categoryOrder, CharSequence title) {
        return this.addInternal(group, id, categoryOrder, title);
    }

    public void removeItem(int id) {
        this.removeItemAtInt(this.findItemIndex(id), true);
    }

    @Override
    public boolean expandItemActionView(MenuItem item) {
        return false;
    }

    @Override
    public boolean collapseItemActionView(MenuItem item) {
        return false;
    }

    @Override
    public void onMenuItemSelected(MenuItem item) {

    }

    @Override
    public void onMenuItemExpand(MenuItem item) {

    }

    @Override
    public void onMenuItemCollapse(MenuItem item) {

    }

    public void removeGroup(int group) {
        int i = this.findGroupIndex(group);
        if (i >= 0) {
            int maxRemovable = this.mItems.size() - i;
            int var4 = 0;

            while (var4++ < maxRemovable && ((MenuItemImpl) this.mItems.get(i)).getGroupId() == group) {
                this.removeItemAtInt(i, false);
            }
        }

    }

    private void removeItemAtInt(int index, boolean updateChildrenOnMenuViews) {
        if (index >= 0 && index < this.mItems.size()) {
            this.mItems.remove(index);
        }
    }

    public void removeItemAt(int index) {
        this.removeItemAtInt(index, true);
    }

    public void clearAll() {
        this.mPreventDispatchingItemsChanged = true;
        this.clear();
        this.mPreventDispatchingItemsChanged = false;
        this.mItemsChangedWhileDispatchPrevented = false;
        this.mStructureChangedWhileDispatchPrevented = false;
    }

    public void clear() {
        if (this.mExpandedItem != null) {
            this.collapseItemActionView(this.mExpandedItem);
        }

        this.mItems.clear();
    }

    @Override
    public void close() {

    }

    public void setGroupCheckable(int group, boolean checkable, boolean exclusive) {
        int N = this.mItems.size();

        for (int i = 0; i < N; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getGroupId() == group) {
                item.setExclusiveCheckable(exclusive);
                item.setCheckable(checkable);
            }
        }

    }

    public void setGroupVisible(int group, boolean visible) {
        int N = this.mItems.size();
        boolean changedAtLeastOneItem = false;

        for (int i = 0; i < N; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getGroupId() == group && item.setVisibleInt(visible)) {
                changedAtLeastOneItem = true;
            }
        }
    }

    public void setGroupEnabled(int group, boolean enabled) {
        int N = this.mItems.size();

        for (int i = 0; i < N; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getGroupId() == group) {
                item.setEnabled(enabled);
            }
        }

    }

    public boolean hasVisibleItems() {
        if (this.mOverrideVisibleItems) {
            return true;
        } else {
            int size = this.size();

            for (int i = 0; i < size; ++i) {
                MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
                if (item.isVisible()) {
                    return true;
                }
            }

            return false;
        }
    }

    public MenuItem findItem(int id) {
        int size = this.size();

        for (int i = 0; i < size; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getItemId() == id) {
                return item;
            }
        }

        return null;
    }

    public int findItemIndex(int id) {
        int size = this.size();

        for (int i = 0; i < size; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getItemId() == id) {
                return i;
            }
        }

        return -1;
    }

    public int findGroupIndex(int group) {
        return this.findGroupIndex(group, 0);
    }

    public int findGroupIndex(int group, int start) {
        int size = this.size();
        if (start < 0) {
            start = 0;
        }

        for (int i = start; i < size; ++i) {
            MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
            if (item.getGroupId() == group) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public void load(int menuRes) {

    }

    public int size() {
        return this.mItems.size();
    }

    public MenuItem getItem(int index) {
        return (MenuItem) this.mItems.get(index);
    }


    public void setQwertyMode(boolean isQwerty) {
        this.mQwertyMode = isQwerty;
    }

    private static int getOrdering(int categoryOrder) {
        int index = (categoryOrder & -65536) >> 16;
        if (index >= 0 && index < sCategoryToOrder.length) {
            return sCategoryToOrder[index] << 16 | categoryOrder & '\uffff';
        } else {
            throw new IllegalArgumentException("order does not contain a valid category.");
        }
    }

    boolean isQwertyMode() {
        return this.mQwertyMode;
    }

    public boolean isShortcutsVisible() {
        return this.mShortcutsVisible;
    }

    public Context getContext() {
        return this.mContext;
    }

    boolean dispatchMenuItemSelected(MenuBuilder menu, MenuItem item) {
        return this.mCallback != null && this.mCallback.onMenuItemSelected(menu, item);
    }

    public void changeMenuMode() {
        if (this.mCallback != null) {
            this.mCallback.onMenuModeChange(this);
        }

    }

    private static int findInsertIndex(ArrayList<MenuItemImpl> items, int ordering) {
        for (int i = items.size() - 1; i >= 0; --i) {
            MenuItemImpl item = (MenuItemImpl) items.get(i);
            if (item.getOrdering() <= ordering) {
                return i + 1;
            }
        }

        return 0;
    }

    public void stopDispatchingItemsChanged() {
        if (!this.mPreventDispatchingItemsChanged) {
            this.mPreventDispatchingItemsChanged = true;
            this.mItemsChangedWhileDispatchPrevented = false;
            this.mStructureChangedWhileDispatchPrevented = false;
        }

    }

    public ArrayList<MenuItemImpl> getVisibleItems() {
        if (!this.mIsVisibleItemsStale) {
            return this.mVisibleItems;
        } else {
            this.mVisibleItems.clear();
            int itemsSize = this.mItems.size();

            for (int i = 0; i < itemsSize; ++i) {
                MenuItemImpl item = (MenuItemImpl) this.mItems.get(i);
                if (item.isVisible()) {
                    this.mVisibleItems.add(item);
                }
            }

            this.mIsVisibleItemsStale = false;
            this.mIsActionItemsStale = true;
            return this.mVisibleItems;
        }
    }

    public CharSequence getHeaderTitle() {
        return this.mHeaderTitle;
    }

    public Element getHeaderIcon() {
        return this.mHeaderIcon;
    }

    public Component getHeaderView() {
        return this.mHeaderView;
    }

    public MenuBuilder getRootMenu() {
        return this;
    }

    public void setCurrentMenuInfo(ContextMenu.ContextMenuInfo menuInfo) {
        this.mCurrentMenuInfo = menuInfo;
    }

    public void setOptionalIconsVisible(boolean visible) {
        this.mOptionalIconsVisible = visible;
    }

    boolean getOptionalIconsVisible() {
        return this.mOptionalIconsVisible;
    }

    public MenuItemImpl getExpandedItem() {
        return this.mExpandedItem;
    }

    public void setOverrideVisibleItems(boolean override) {
        this.mOverrideVisibleItems = override;
    }

    public interface ItemInvoker {
        boolean invokeItem(MenuItemImpl var1);
    }

    public interface Callback {
        boolean onMenuItemSelected(MenuBuilder var1, MenuItem var2);

        void onMenuModeChange(MenuBuilder var1);
    }
}
