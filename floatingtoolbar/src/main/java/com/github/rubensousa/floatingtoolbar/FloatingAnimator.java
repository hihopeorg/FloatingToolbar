/*
 * Copyright 2016 Rúben Sousa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.Image;

abstract class FloatingAnimator {

    public static final int DELAY_MIN_WIDTH = 300;
    public static final int DELAY_MAX_WIDTH = 900;
    public static final int DELAY_MAX = 150;
    public static final int FAB_MORPH_DURATION = 200;
    public static final int FAB_UNMORPH_DURATION = 200;
    public static final int FAB_UNMORPH_DELAY = 300;
    public static final int CIRCULAR_REVEAL_DURATION = 300;
    public static final int CIRCULAR_UNREVEAL_DURATION = 200;
    public static final int CIRCULAR_REVEAL_DELAY = 50;
    public static final int CIRCULAR_UNREVEAL_DELAY = 150;
    public static final int TOOLBAR_UNREVEAL_DELAY = 200;
    public static final int MENU_ANIMATION_DELAY = 200;
    public static final int MENU_ANIMATION_DURATION = 300;

    private float mAppbarOffset;
    private Image mFab;
    private FloatingToolbar mToolbar;
    private Component mContentView;
    private long mDelay;
    private boolean mMoveFabX;
    public FloatingAnimatorListener mAnimationListener;
    public Snackbar mSnackbar;

    public FloatingAnimator(FloatingToolbar toolbar) {
        mToolbar = toolbar;
    }

    public void setFab(Image fab) {
        mFab = fab;
        mFab.getComponentTreeObserver().addTreeLayoutChangedListener(new ComponentTreeObserver.GlobalLayoutListener() {
            @Override
            public void onGlobalLayoutUpdated() {
                if (mToolbar.getWidth() != 0) {
                    mMoveFabX = mFab.getRight() > mToolbar.getWidth() * 0.75
                            || mFab.getLeft() < mToolbar.getHeight() * 0.25;
                    mFab.getComponentTreeObserver().removeTreeLayoutChangedListener(this);
                }
            }
        });
    }

    public Image getFab() {
        return mFab;
    }

    public FloatingToolbar getFloatingToolbar() {
        return mToolbar;
    }

    public void setFloatingAnimatorListener(FloatingAnimatorListener listener) {
        mAnimationListener = listener;
    }

    public FloatingAnimatorListener getAnimationListener() {
        return mAnimationListener;
    }

    public void setSnackBar(Snackbar snackbar) {
        mSnackbar = snackbar;
    }

    public void setUIContent(Component contentView) {
        mContentView = contentView;
    }

    public float getAppBarOffset() {
        return mAppbarOffset;
    }

    public long getDelay() {
        return mDelay;
    }

    public boolean shouldMoveFabX() {
        return mMoveFabX;
    }

    public void show() {
        if (mMoveFabX) {
            float fabEndX = mFab.getLeft() > mToolbar.getWidth() / 2f ?
                    mFab.getLeft() - mFab.getWidth() : mFab.getLeft() + mFab.getWidth();

            // Place view a bit closer to the fab
            mToolbar.setTranslationX(fabEndX - mToolbar.getWidth() / 2f + mFab.getWidth());

            // Move FloatingToolbar to the original position
            mToolbar.createAnimatorProperty().moveByX(mToolbar.getLeft())
                    .setDelay(CIRCULAR_REVEAL_DELAY + mDelay)
                    .setDuration((long) (CIRCULAR_REVEAL_DURATION) + mDelay);
        }

        // Start showing content view
        if (mContentView != null) {
            mContentView.setAlpha(255);
            mContentView.setScaleX(0.7f);
            mContentView.createAnimatorProperty().alpha(255).scaleX(1f)
                    .setDuration(MENU_ANIMATION_DURATION + mDelay)
                    .setDelay(MENU_ANIMATION_DELAY + mDelay)
                    .setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {

                        }

                        @Override
                        public void onStop(Animator animator) {

                        }

                        @Override
                        public void onCancel(Animator animator) {

                        }

                        @Override
                        public void onEnd(Animator animator) {
                            getAnimationListener().onAnimationFinished();
                            mContentView.createAnimatorProperty().setStateChangedListener(null);
                        }

                        @Override
                        public void onPause(Animator animator) {

                        }

                        @Override
                        public void onResume(Animator animator) {

                        }
                    });
        }
    }

    public void hide() {
        if (mMoveFabX) {
            mToolbar.createAnimatorProperty().moveByX(mFab.getLeft() - mToolbar.getWidth() / 2f)
                    .setDuration(CIRCULAR_UNREVEAL_DURATION + mDelay)
                    .setDelay(TOOLBAR_UNREVEAL_DELAY + mDelay)
                    .setStateChangedListener(null);
        }
        if (mContentView != null) {
            mContentView.createAnimatorProperty().alpha(0f).scaleX(0.7f)
                    .setDelay(CIRCULAR_UNREVEAL_DELAY + mDelay)
                    .setDuration((MENU_ANIMATION_DURATION / 2) + mDelay)
                    .setStateChangedListener(null);
        }
    }

    /**
     * Calculate a delay that depends on the screen width so that animations don't happen too quick
     * on larger phones or tablets
     * <p>
     * Base is 300dp.
     * <p>
     * A root view with 300dp as width has 0 delay
     * <p>
     * The max width is 1200dp, with a max delay of 200 ms
     */
    public void updateDelay() {
        float minWidth = FloatingToolbar.dpToPixels(mToolbar.getContext(), DELAY_MIN_WIDTH);
        float maxWidth = FloatingToolbar.dpToPixels(mToolbar.getContext(), DELAY_MAX_WIDTH);
        float diff = maxWidth - minWidth;

        int width = mToolbar.getWidth();

        if (width == 0 || width < minWidth) {
            mDelay = 0;
            return;
        }

        if (width > maxWidth) {
            mDelay = DELAY_MAX;
            return;
        }

        mDelay = (long) (DELAY_MAX / diff * (width - minWidth));
    }

    interface FloatingAnimatorListener {
        void onAnimationFinished();
    }
}
