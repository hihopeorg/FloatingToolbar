/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class Snackbar extends ComponentContainer {
    public Snackbar(Context context) {
        super(context);
        initView();
    }

    public Snackbar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView();
    }

    public Snackbar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {

        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, 120);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(54, 54, 54));
        setBackground(shapeElement);

        Text text = new Text(getContext());
        text.setText("Here's a SnackBar");
        text.setTextSize(16, Text.TextSizeType.FP);
        text.setLayoutConfig(layoutConfig);
        text.setBackground(shapeElement);
        text.setPaddingLeft(50);
        text.setTextColor(Color.WHITE);

        addComponent(text);
    }
}
