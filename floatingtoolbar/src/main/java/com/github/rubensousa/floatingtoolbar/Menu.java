/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;


public interface Menu {

    static final int NONE = 0;

    void load(int menuRes);

    int size();

    boolean hasVisibleItems();

    MenuItem findItem(int id);

    MenuItem getItem(int index);

    void removeItem(int id);

    boolean expandItemActionView(MenuItem item);

    boolean collapseItemActionView(MenuItem item);

    void onMenuItemSelected(MenuItem item);

    void onMenuItemExpand(MenuItem item);

    void onMenuItemCollapse(MenuItem item);

    void clear();

    void close();
}
