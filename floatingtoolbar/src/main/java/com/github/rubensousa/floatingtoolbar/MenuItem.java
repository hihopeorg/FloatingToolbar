/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

public interface MenuItem {

    interface OnMenuItemClickListener {
        boolean onMenuItemClick(MenuItem item);
    }

    interface OnActionExpandListener {
        boolean onMenuItemActionExpand(MenuItem item);

        boolean onMenuItemActionCollapse(MenuItem item);
    }

    MenuItem setItemId(int id);

    int getItemId();

    MenuItem setTitle(CharSequence title);

    MenuItem setTitle(int titleRes);

    CharSequence getTitle();

    MenuItem setIcon(Element icon);

    MenuItem setIcon(int iconRes);

    Element getIcon();

    MenuItem setVisible(boolean visible);

    boolean isVisible();

    MenuItem setEnable(boolean enable);

    boolean isEnable();

    boolean isExpandable();

    MenuItem setOnMenuItemClickListener(OnMenuItemClickListener menuItemClickListener);

    MenuItem setActionView(int layoutRes);

    Component getActionView();

    boolean expandActionView();

    boolean collapseActionView();

    boolean isActionViewExpanded();

    MenuItem setOnActionExpandListener(OnActionExpandListener listener);

}
