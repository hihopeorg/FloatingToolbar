/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

public class MenuItemImpl implements SupportMenuItem {
    private static final String TAG = "MenuItemImpl";
    private static final int SHOW_AS_ACTION_MASK = 3;
    private final int mId;
    private final int mGroup;
    private final int mCategoryOrder;
    private final int mOrdering;
    private CharSequence mTitle;
    private CharSequence mTitleCondensed;
    private Intent mIntent;
    private char mShortcutNumericChar;
    private int mShortcutNumericModifiers = 4096;
    private char mShortcutAlphabeticChar;
    private int mShortcutAlphabeticModifiers = 4096;
    private Element mIconDrawable;
    private int mIconResId = 0;
    MenuBuilder mMenu;
    private Runnable mItemCallback;
    private OnMenuItemClickListener mClickListener;
    private CharSequence mContentDescription;
    private CharSequence mTooltipText;

    private boolean mHasIconTint = false;
    private boolean mHasIconTintMode = false;
    private boolean mNeedToApplyIconTint = false;
    private int mFlags = 16;
    private static final int CHECKABLE = 1;
    private static final int CHECKED = 2;
    private static final int EXCLUSIVE = 4;
    private static final int HIDDEN = 8;
    private static final int ENABLED = 16;
    private static final int IS_ACTION = 32;
    private int mShowAsAction = 0;
    private Component mActionView;
    private OnActionExpandListener mOnActionExpandListener;
    private boolean mIsActionViewExpanded = false;
    static final int NO_ICON = 0;
    private ContextMenu.ContextMenuInfo mMenuInfo;
    private static String sPrependShortcutLabel;
    private static String sEnterShortcutLabel;
    private static String sDeleteShortcutLabel;
    private static String sSpaceShortcutLabel;

    MenuItemImpl(MenuBuilder menu, int group, int id, int categoryOrder, int ordering, CharSequence title, int showAsAction) {
        this.mMenu = menu;
        this.mId = id;
        this.mGroup = group;
        this.mCategoryOrder = categoryOrder;
        this.mOrdering = ordering;
        this.mTitle = title;
        this.mShowAsAction = showAsAction;
    }

    public boolean invoke() {
        if (this.mClickListener != null && this.mClickListener.onMenuItemClick(this)) {
            return true;
        } else if (this.mMenu.dispatchMenuItemSelected(this.mMenu, this)) {
            return true;
        } else if (this.mItemCallback != null) {
            this.mItemCallback.run();
            return true;
        } else {
            return true;
        }
    }

    public boolean isEnabled() {
        return (this.mFlags & 16) != 0;
    }

    public MenuItem setEnabled(boolean enabled) {
        if (enabled) {
            this.mFlags |= 16;
        } else {
            this.mFlags &= -17;
        }
        return this;
    }

    public int getGroupId() {
        return this.mGroup;
    }

    @Override
    public MenuItem setItemId(int id) {
        return null;
    }

    public int getItemId() {
        return this.mId;
    }

    public int getOrder() {
        return this.mCategoryOrder;
    }

    public int getOrdering() {
        return this.mOrdering;
    }

    public Intent getIntent() {
        return this.mIntent;
    }

    public MenuItem setIntent(Intent intent) {
        this.mIntent = intent;
        return this;
    }

    Runnable getCallback() {
        return this.mItemCallback;
    }

    public MenuItem setCallback(Runnable callback) {
        this.mItemCallback = callback;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.mShortcutAlphabeticChar;
    }

    public MenuItem setAlphabeticShortcut(char alphaChar) {
        if (this.mShortcutAlphabeticChar == alphaChar) {
            return this;
        } else {
            this.mShortcutAlphabeticChar = Character.toLowerCase(alphaChar);
            return this;
        }
    }

    public MenuItem setAlphabeticShortcut(char alphaChar, int alphaModifiers) {
        if (this.mShortcutAlphabeticChar == alphaChar && this.mShortcutAlphabeticModifiers == alphaModifiers) {
            return this;
        } else {
            this.mShortcutAlphabeticChar = Character.toLowerCase(alphaChar);
            return this;
        }
    }

    public int getAlphabeticModifiers() {
        return this.mShortcutAlphabeticModifiers;
    }

    public char getNumericShortcut() {
        return this.mShortcutNumericChar;
    }

    public int getNumericModifiers() {
        return this.mShortcutNumericModifiers;
    }

    public MenuItem setNumericShortcut(char numericChar) {
        if (this.mShortcutNumericChar == numericChar) {
            return this;
        } else {
            this.mShortcutNumericChar = numericChar;
            return this;
        }
    }

    public MenuItem setNumericShortcut(char numericChar, int numericModifiers) {
        if (this.mShortcutNumericChar == numericChar && this.mShortcutNumericModifiers == numericModifiers) {
            return this;
        } else {
            this.mShortcutNumericChar = numericChar;
            return this;
        }
    }

    public MenuItem setShortcut(char numericChar, char alphaChar) {
        this.mShortcutNumericChar = numericChar;
        this.mShortcutAlphabeticChar = Character.toLowerCase(alphaChar);
        return this;
    }

    public MenuItem setShortcut(char numericChar, char alphaChar, int numericModifiers, int alphaModifiers) {
        this.mShortcutNumericChar = numericChar;
        this.mShortcutAlphabeticChar = Character.toLowerCase(alphaChar);
        return this;
    }

    char getShortcut() {
        return this.mMenu.isQwertyMode() ? this.mShortcutAlphabeticChar : this.mShortcutNumericChar;
    }

    String getShortcutLabel() {
        char shortcut = this.getShortcut();
        if (shortcut == 0) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder(sPrependShortcutLabel);
            switch (shortcut) {
                case '\b':
                    sb.append(sDeleteShortcutLabel);
                    break;
                case '\n':
                    sb.append(sEnterShortcutLabel);
                    break;
                case ' ':
                    sb.append(sSpaceShortcutLabel);
                    break;
                default:
                    sb.append(shortcut);
            }

            return sb.toString();
        }
    }

    boolean shouldShowShortcut() {
        return this.mMenu.isShortcutsVisible() && this.getShortcut() != 0;
    }

    public CharSequence getTitle() {
        return this.mTitle;
    }

    public MenuItem setTitle(CharSequence title) {
        this.mTitle = title;
        return this;
    }

    public MenuItem setTitle(int title) {
        return this.setTitle(this.mMenu.getContext().getString(title));
    }

    public CharSequence getTitleCondensed() {
        CharSequence ctitle = this.mTitleCondensed != null ? this.mTitleCondensed : this.mTitle;
        return (CharSequence) (ctitle != null && !(ctitle instanceof String) ? ctitle.toString() : ctitle);
    }

    public MenuItem setTitleCondensed(CharSequence title) {
        this.mTitleCondensed = title;
        if (title == null) {
            title = this.mTitle;
        }
        return this;
    }

    public Element getIcon() {
        if (this.mIconDrawable != null) {
            return this.applyIconTintIfNecessary(this.mIconDrawable);
        } else if (this.mIconResId != 0) {
            this.mIconResId = 0;
            return mIconDrawable;
        } else {
            return null;
        }
    }

    public MenuItem setIcon(Element icon) {
        this.mIconResId = 0;
        this.mIconDrawable = icon;
        this.mNeedToApplyIconTint = true;
        return this;
    }

    public MenuItem setIcon(int iconResId) {
        this.mIconDrawable = null;
        this.mIconResId = iconResId;
        this.mNeedToApplyIconTint = true;
        return this;
    }

    private Element applyIconTintIfNecessary(Element icon) {
        if (icon != null && this.mNeedToApplyIconTint && (this.mHasIconTint || this.mHasIconTintMode)) {
            this.mNeedToApplyIconTint = false;
        }

        return icon;
    }

    public boolean isCheckable() {
        return (this.mFlags & 1) == 1;
    }

    public MenuItem setCheckable(boolean checkable) {
        int oldFlags = this.mFlags;
        this.mFlags = this.mFlags & -2 | (checkable ? 1 : 0);
        return this;
    }

    public void setExclusiveCheckable(boolean exclusive) {
        this.mFlags = this.mFlags & -5 | (exclusive ? 4 : 0);
    }

    public boolean isExclusiveCheckable() {
        return (this.mFlags & 4) != 0;
    }

    public boolean isChecked() {
        return (this.mFlags & 2) == 2;
    }

    public MenuItem setChecked(boolean checked) {
        if ((this.mFlags & 4) != 0) {

        } else {
            this.setCheckedInt(checked);
        }

        return this;
    }

    void setCheckedInt(boolean checked) {
        int oldFlags = this.mFlags;
        this.mFlags = this.mFlags & -3 | (checked ? 2 : 0);
    }

    boolean setVisibleInt(boolean shown) {
        int oldFlags = this.mFlags;
        this.mFlags = this.mFlags & -9 | (shown ? 0 : 8);
        return oldFlags != this.mFlags;
    }

    public MenuItem setVisible(boolean shown) {
        if (this.setVisibleInt(shown)) {

        }
        return this;
    }

    @Override
    public boolean isVisible() {
        return false;
    }

    @Override
    public MenuItem setEnable(boolean enable) {
        return null;
    }

    @Override
    public boolean isEnable() {
        return false;
    }

    @Override
    public boolean isExpandable() {
        return false;
    }

    public MenuItem setOnMenuItemClickListener(OnMenuItemClickListener clickListener) {
        this.mClickListener = clickListener;
        return this;
    }

    public String toString() {
        return this.mTitle != null ? this.mTitle.toString() : null;
    }

    void setMenuInfo(ContextMenu.ContextMenuInfo menuInfo) {
        this.mMenuInfo = menuInfo;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.mMenuInfo;
    }

    public boolean shouldShowIcon() {
        return this.mMenu.getOptionalIconsVisible();
    }

    public boolean isActionButton() {
        return (this.mFlags & 32) == 32;
    }

    public boolean requestsActionButton() {
        return (this.mShowAsAction & 1) == 1;
    }

    public boolean requiresActionButton() {
        return (this.mShowAsAction & 2) == 2;
    }

    public void setIsActionButton(boolean isActionButton) {
        if (isActionButton) {
            this.mFlags |= 32;
        } else {
            this.mFlags &= -33;
        }

    }

    public boolean showsTextAsAction() {
        return (this.mShowAsAction & 4) == 4;
    }

    public void setShowAsAction(int actionEnum) {
        switch (actionEnum & 3) {
            case 0:
            case 1:
            case 2:
                this.mShowAsAction = actionEnum;
                return;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
    }

    public SupportMenuItem setActionView(Component view) {
        this.mActionView = view;
        if (view != null && view.getId() == -1 && this.mId > 0) {
            view.setId(this.mId);
        }
        return this;
    }

    @Override
    public MenuItem setActionView(int var1) {
        return null;
    }

    public Component getActionView() {
        if (this.mActionView != null) {
            return this.mActionView;
        } else {
            return null;
        }
    }

    public SupportMenuItem setShowAsActionFlags(int actionEnum) {
        this.setShowAsAction(actionEnum);
        return this;
    }

    public boolean expandActionView() {
        if (!this.hasCollapsibleActionView()) {
            return false;
        } else {
            return this.mOnActionExpandListener != null && !this.mOnActionExpandListener.onMenuItemActionExpand(this) ? false : this.mMenu.expandItemActionView(this);
        }
    }

    public boolean collapseActionView() {
        if ((this.mShowAsAction & 8) == 0) {
            return false;
        } else if (this.mActionView == null) {
            return true;
        } else {
            return this.mOnActionExpandListener != null && !this.mOnActionExpandListener.onMenuItemActionCollapse(this) ? false : this.mMenu.collapseItemActionView(this);
        }
    }

    public boolean hasCollapsibleActionView() {
        if ((this.mShowAsAction & 8) != 0) {
            return this.mActionView != null;
        } else {
            return false;
        }
    }

    public void setActionViewExpanded(boolean isExpanded) {
        this.mIsActionViewExpanded = isExpanded;
    }

    public boolean isActionViewExpanded() {
        return this.mIsActionViewExpanded;
    }

    public MenuItem setOnActionExpandListener(OnActionExpandListener listener) {
        this.mOnActionExpandListener = listener;
        return this;
    }

    public SupportMenuItem setContentDescription(CharSequence contentDescription) {
        this.mContentDescription = contentDescription;
        return this;
    }

    public CharSequence getContentDescription() {
        return this.mContentDescription;
    }

    public SupportMenuItem setTooltipText(CharSequence tooltipText) {
        this.mTooltipText = tooltipText;
        return this;
    }

    public CharSequence getTooltipText() {
        return this.mTooltipText;
    }
}

