/*
 * Copyright 2016 Rúben Sousa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;


class FloatingAnimatorLollipopImpl extends FloatingAnimator {

    FloatingAnimatorLollipopImpl(FloatingToolbar toolbar) {
        super(toolbar);
    }

    @Override
    public void show() {
        if (null != mSnackbar) {
            mSnackbar.setVisibility(Component.HIDE);
        }
        AnimatorProperty anim = getFab().createAnimatorProperty();
        anim.alphaFrom(1).alpha(0).setDuration(200);
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                getFloatingToolbar().createAnimatorProperty()
                        .moveFromX(getFloatingToolbar().getWidth()).moveToX(0)
                        .alphaFrom(0f).alpha(1.0f)
                        .setDuration(500)
                        .start();
                getFab().setVisibility(Component.HIDE);
                mAnimationListener.onAnimationFinished();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    @Override
    public void hide() {
        if (null != mSnackbar) {
            mSnackbar.setVisibility(Component.HIDE);
        }
        AnimatorProperty anim = new AnimatorProperty();
        anim.setTarget(getFloatingToolbar());
        anim.moveFromX(0).moveToX(getFloatingToolbar().getWidth())
                .alphaFrom(1.0f).alpha(0f)
                .setDuration(500);

        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                getFab().setVisibility(Component.VISIBLE);
                getFab().createAnimatorProperty().alphaFrom(0).alpha(1).setDuration(200).start();
                getAnimationListener().onAnimationFinished();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

}
