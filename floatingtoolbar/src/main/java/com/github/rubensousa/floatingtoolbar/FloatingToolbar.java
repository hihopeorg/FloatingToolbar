/*
 * Copyright 2016 Rúben Sousa
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("RestrictedApi")
public class FloatingToolbar extends DirectionalLayout implements Component.ClickedListener,
        Component.LongClickedListener, FloatingAnimator.FloatingAnimatorListener {

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    private int mMenuRes;

    private int mItemBackground;

    private Component.ScrolledListener mScrollListener;
    private ListContainer mRecyclerView;

    Image mFab;
    private Component mCustomView;
    private Menu mMenu;
    boolean mShowing;
    boolean mAnimating;
    boolean mHandleFabClick;
    private boolean mAutoHide;
    private boolean mShowToast;
    private ToastDialog mToast;
    private ItemClickListener mClickListener;
    private DirectionalLayout mMenuLayout;
    private FloatingAnimator mAnimator;
    private List<MorphListener> mMorphListeners;
    private FloatingSnackBarManager mSnackBarManager;
    private Snackbar mSnackbar;

    private ClickedListener mViewClickListener = new ClickedListener() {
        @Override
        public void onClick(Component view) {
            if (!mShowing && mHandleFabClick) {
                show();
            }
        }
    };

    public FloatingToolbar(Context context) {
        this(context, null, 0);
    }

    public FloatingToolbar(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public FloatingToolbar(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, "");

        // Set colorAccent as default color
        if (getBackgroundElement() == null) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(0, 0, 255));
            setBackground(shapeElement);
        }

        mMorphListeners = new ArrayList<>();
        mScrollListener = new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                if (isShowing()) {
                    hide();
                }
            }
        };
        mShowToast = true;
        mHandleFabClick = true;
        mItemBackground = Color.BLUE.getValue();
        mAutoHide = true;
        mMenuRes = 0;

        int customView = 0;

        if (customView != 0) {
            mCustomView = LayoutScatter.getInstance(context).parse(customView, this, false);
        }

        mAnimator = new FloatingAnimatorLollipopImpl(this);
        if (mCustomView != null) {
            addComponent(mCustomView);
            mAnimator.setUIContent(mCustomView);
        }

        if (mMenuRes != 0 && customView == 0) {
            createMenuLayout();
            addMenuItems();
            mAnimator.setUIContent(mMenuLayout);
        }

        setAlpha(0f);
        setOrientation(HORIZONTAL);
        mSnackBarManager = new FloatingSnackBarManager(this);
    }

    /**
     * Check if the FloatingToolbar is being shown due a previous FloatingActionButton click
     * or a manual call to {@link #show() show()}
     *
     * @return true if the FloatingToolbar is being shown and the fab is hidden
     */
    public boolean isShowing() {
        return mShowing;
    }

    /**
     * Control whether the FloatingToolbar should handle fab clicks or force manual calls
     * to {@link #show() show}
     *
     * @param handle true if this FloatingToolbar should be shown automatically on fab click
     */
    public void handleFabClick(boolean handle) {
        mHandleFabClick = handle;
        if (mHandleFabClick && mFab != null) {
            mFab.setClickedListener(mViewClickListener);
        }
    }

    /**
     * Enable or disable auto hide when a menu item is clicked. The default value is true.
     *
     * @param enable true if you want to enable auto hide
     */
    public void enableAutoHide(boolean enable) {
        mAutoHide = enable;
    }

    /**
     * @return true if the FloatingToolbar is being shown automatically
     * by handling FloatingActionButton clicks.
     */
    public boolean isHandlingFabClick() {
        return mHandleFabClick;
    }

    /**
     * Set a ItemClickListener that'll receive item click events from the Component built from a Menu.
     *
     * @param listener Listener that'll receive item click events
     */
    public void setClickListener(ItemClickListener listener) {
        mClickListener = listener;
    }

    /**
     * @return The custom view associated to this FloatingToolbar, or null if there's none.
     */
    public Component getCustomView() {
        return mCustomView;
    }

    /**
     * @return The menu associated to this FloatingToolbar, or null if there's none
     */
    public Menu getMenus() {
        return mMenu;
    }

    /**
     * Place a view inside this FlootingToolbar. It'll be animated automatically.
     *
     * @param view Component to be shown inside this FloatingToolbar
     */
    public void setCustomView(Component view) {
        removeAllComponents();
        mCustomView = view;
        mAnimator.setUIContent(mCustomView);
        addComponent(view);
    }

    /**
     * Set a menu from it's resource id.
     *
     * @param menuRes menu resource to be set
     */
    public void setMenu(int menuRes) {
        mMenu = new MenuBuilder(getContext());
        setMenu(mMenu);
    }

    /**
     * Set a menu that'll be used to show a set of options using icons
     *
     * @param menu menu to be set
     */
    public void setMenu(Menu menu) {
        mMenu = menu;
        if (mMenuLayout == null) {
            createMenuLayout();
        }
        mMenuLayout.removeAllComponents();
        addMenuItems();
        mAnimator.setUIContent(mMenuLayout);
    }

    public Menu getMenu() {
        return mMenu;
    }

    /**
     * Attach a FloatingActionButton that'll be used for the morph animation.
     * <p>It will be hidden after {@link #show() show()} is called
     * and shown after {@link #hide() hide()} is called.
     * </p>
     *
     * @param fab FloatingActionButton to attach
     */
    public void attachFab(Image fab) {
        if (fab == null) {
            return;
        }
        mFab = fab;
        mAnimator.setFab(mFab);
        mAnimator.setFloatingAnimatorListener(this);
        if (mHandleFabClick) {
            mFab.setClickedListener(mViewClickListener);
        }
    }

    /**
     * Detach the FloatingActionButton from this FloatingToolbar.
     * <p>This will disable the auto morph on click.</p>
     */
    public void detachFab() {
        mAnimator.setFab(null);
        if (mFab != null) {
            mFab.setClickedListener(null);
            mFab = null;
        }
    }

    /**
     * Attach a RecyclerView to hide this FloatingToolbar automatically when a scroll is detected.
     *
     * @param recyclerView RecyclerView to listen for scroll events
     */
    public void attachRecyclerView(ListContainer recyclerView) {
        if (recyclerView != null) {
            mRecyclerView = recyclerView;
            mRecyclerView.addScrolledListener(mScrollListener);
        }
    }

    /**
     * Detach the current RecyclerView to stop hiding automatically this FloatingToolbar
     * when a scroll is detected.
     */
    public void detachRecyclerView() {
        if (mRecyclerView != null) {
            mRecyclerView = null;
        }
    }

    /**
     * Add a morph listener to listen for animation events
     *
     * @param listener MorphListener to be added
     */
    public void addMorphListener(MorphListener listener) {
        if (!mMorphListeners.contains(listener)) {
            mMorphListeners.add(listener);
        }
    }

    /**
     * Remove a morph listener previous added
     *
     * @param listener MorphListener to be removed
     */
    public void removeMorphListener(MorphListener listener) {
        mMorphListeners.remove(listener);
    }

    /**
     * Removes every morph listener previous added
     */
    public void removeMorphListeners() {
        mMorphListeners.clear();
    }

    /**
     * This method will automatically morph the attached FloatingActionButton
     * into this FloatingToolbar.
     *
     * @throws IllegalStateException if there's no FloatingActionButton attached
     */
    public void show() {
        if (mFab == null) {
            throw new IllegalStateException("FloatingActionButton not attached." +
                    "Please, use attachFab(FloatingActionButton fab).");
        }
        if (mAnimating || mShowing) {
            return;
        }
        dispatchShow();
    }

    /**
     * This method will automatically morph the FloatingToolbar into the attached FloatingActionButton
     *
     * @throws IllegalStateException if there's no FloatingActionButton attached
     */
    public void hide() {
        if (mFab == null) {
            throw new IllegalStateException("FloatingActionButton not attached." +
                    "Please, use attachFab(FloatingActionButton fab).");
        }

        if (mShowing && !mAnimating) {
            dispatchHide();
        }
    }

    @Override
    public void onClick(Component v) {
        if (!mShowing || mAnimating) {
            return;
        }

        if (mAutoHide) {
            hide();
        }

        if (mClickListener != null) {
            MenuItem item = (MenuItem) v.getTag();
            mClickListener.onItemClick(item);
        }
    }

    @Override
    public void onAnimationFinished() {
        mAnimating = false;
        if (!mShowing) {
            for (MorphListener morphListener : mMorphListeners) {
                morphListener.onUnmorphEnd();
            }
        } else {
            for (MorphListener morphListener : mMorphListeners) {
                morphListener.onMorphEnd();
            }
        }
    }

    public void setSnackBar(Snackbar snackbar) {
        mSnackbar = snackbar;
    }


    /**
     * Show a snackbar behind the floating toolbar if it's showing
     *
     * @param snackbar Snackbar to be shown
     */
    public void showSnackBar(Snackbar snackbar) {
        mSnackBarManager.showSnackBar(snackbar);
    }

    /**
     * Place the menu items with icons inside a horizontal LinearLayout
     */
    private void addMenuItems() {

        if (mMenu == null) {
            mMenu = new MenuBuilder(getContext());
        }

        DirectionalLayout.LayoutConfig layoutParams
                = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT, LayoutAlignment.CENTER, 1);
        setTotalWeight(mMenu.size());

        for (int i = 0; i < mMenu.size(); i++) {
            DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
            MenuItem item = mMenu.getItem(i);
            Image imageButton = new Image(getContext());
            //noinspection ResourceType
            imageButton.setId(item.getItemId() == Menu.NONE ? genViewId() : item.getItemId());
            imageButton.setImageElement(item.getIcon());
            imageButton.setClickedListener(this);
            imageButton.setLongClickedListener(this);
            imageButton.setTag(item);
            directionalLayout.setAlignment(LayoutAlignment.CENTER);
            directionalLayout.setLayoutConfig(layoutParams);
            directionalLayout.addComponent(imageButton);
            mMenuLayout.addComponent(directionalLayout);
        }
    }

    private void createMenuLayout() {
        mMenuLayout = new DirectionalLayout(getContext());
        DirectionalLayout.LayoutConfig layoutParams
                = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);

        mMenuLayout.setId(genViewId());
        mMenuLayout.setOrientation(ComponentContainer.HORIZONTAL);
        addComponent(mMenuLayout, layoutParams);
        mMenuLayout.setPadding(0, 0, 0, 0);
    }

    void dispatchShow() {
        mShowing = true;
        mAnimator.setSnackBar(mSnackbar);
        if (getWidth() == 0 && getHeight() == 0) {
            setVisibility(Component.VISIBLE);
            if (mFab != null) {
                mFab.setVisibility(Component.INVISIBLE);
            }
        } else {
            mAnimating = true;
            mAnimator.show();
            for (MorphListener morphListener : mMorphListeners) {
                morphListener.onMorphStart();
            }
        }
    }

    void dispatchHide() {
        mShowing = false;
        if (getWidth() == 0 && getHeight() == 0) {
            setVisibility(Component.INVISIBLE);
            if (mFab != null) {
                mFab.setVisibility(Component.VISIBLE);
            }
        } else {
            mAnimator.hide();
            mAnimating = true;
            for (MorphListener morphListener : mMorphListeners) {
                morphListener.onUnmorphStart();
            }
        }
    }

    @Override
    public void onLongClicked(Component component) {
        if (!mShowing || mAnimating) {
            return;
        }

        if (mClickListener != null) {
            MenuItem item = (MenuItem) component.getTag();
            if (mShowToast) {
                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = new ToastDialog(getContext());
                mToast.setText(item.getTitle().toString());
                mToast.setAlignment(LayoutAlignment.BOTTOM);
                mToast.setOffset(0, (int) (getHeight() * 1.25f));
                mToast.show();
            }
            mClickListener.onItemLongClick(item);
            return;
        } else {
            return;
        }
    }

    /**
     * Interface to listen to click events on views with MenuItems.
     * <p>
     * Each method only gets called once, even if the user spams multiple clicks
     * </p>
     */
    public interface ItemClickListener {
        void onItemClick(MenuItem item);

        void onItemLongClick(MenuItem item);
    }

    /**
     * Interface to listen to the morph animation
     */
    public interface MorphListener {
        void onMorphEnd();

        void onMorphStart();

        void onUnmorphStart();

        void onUnmorphEnd();
    }

    // Generate unique view ids to save state properly
    private int genViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF)
                newValue = 1;
            if (sNextGeneratedId.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

    static float dpToPixels(Context context, int dp) {
        return dp * DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().densityPixels;
    }

    public DirectionalLayout getMenuLayout() {
        return mMenuLayout;
    }
}
