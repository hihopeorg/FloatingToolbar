/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.rubensousa.floatingtoolbar;

import ohos.agp.components.Component;

public interface SupportMenuItem extends MenuItem {
    int SHOW_AS_ACTION_NEVER = 0;
    int SHOW_AS_ACTION_IF_ROOM = 1;
    int SHOW_AS_ACTION_ALWAYS = 2;
    int SHOW_AS_ACTION_WITH_TEXT = 4;
    int SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW = 8;

    void setShowAsAction(int var1);

    MenuItem setShowAsActionFlags(int var1);

    MenuItem setActionView(Component var1);

    MenuItem setActionView(int var1);

    Component getActionView();

    boolean expandActionView();

    boolean collapseActionView();

    boolean isActionViewExpanded();

    SupportMenuItem setContentDescription(CharSequence var1);

    CharSequence getContentDescription();

    SupportMenuItem setTooltipText(CharSequence var1);

    CharSequence getTooltipText();

    MenuItem setShortcut(char var1, char var2, int var3, int var4);

    MenuItem setNumericShortcut(char var1, int var2);

    int getNumericModifiers();

    MenuItem setAlphabeticShortcut(char var1, int var2);

    int getAlphabeticModifiers();

}
